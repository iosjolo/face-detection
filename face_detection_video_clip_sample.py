import os
import cv2
import face_recognition

from resources import videos as videos

casc_path = 'haarcascade_frontalface_default.xml'

def face_detection_video_clip(video):
    face_image = face_recognition.load_image_file(video[2])
    model_face_encoding = face_recognition.face_encodings(face_image)[0]

    known_faces = [
        model_face_encoding
    ]

    model_name = video[3]

    movie_capture = cv2.VideoCapture(video[1])
    frame_number = 0

    font = cv2.FONT_HERSHEY_SIMPLEX

    output_folder = 'output'

    while True:
        ret, frame = movie_capture.read()
        frame_number += 1

        if not ret:
            break
        if frame is not None:
            rgb_frame = frame[:, :, ::-1]
            face_locations = face_recognition.face_locations(rgb_frame)
            face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
            face_names = []
            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                match = face_recognition.compare_faces(known_faces, face_encoding, tolerance=0.50)
                if match[0]:
                    name = model_name
                    color = (0, 128, 0)
                    # write frame to file
                    path = os.path.join(output_folder, "frame{:d}.jpg".format(frame_number))
                    cv2.imwrite(path, frame)
                else:
                    name = "Unknow"
                    color = (0, 0, 255)
                face_names.append(name)
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                if not name:
                    continue
                cv2.rectangle(frame, (left, top), (right, bottom), color, 1)
                cv2.rectangle(frame, (left, bottom + 25), (right, bottom), color, cv2.FILLED)
                cv2.putText(frame, name, (left + 6, bottom + 19), font, 0.5, (255, 255, 255), 1)

            cv2.imshow("Video", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    movie_capture.release()
    cv2.destroyAllWindows()
    print("[ END ]")

BOLD = '\033[1m'
CGREENBG2 = '\33[34m'
CEND = '\33[0m'


def begin():
    if __name__ == "__main__":

        os.system('clear')
        print("[ FACE DETECTION VIDEO ]")
        print("-------------------------------------")
        for i, x in enumerate(videos):
            print("[ {} ] : {}".format(i, x[0]))
        print("-------------------------------------")
        # input index of
        idx = int(input(CGREENBG2 + BOLD + "--> PLEASE ENTER VIDEO NUMBER: " + CEND))
        print("[ BEGIN ]")

        video = videos[idx]
        face_detection_video_clip(video)


begin()
