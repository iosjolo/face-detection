import os
videos = [
    ["Capturing Kong", "resources/video/Capturing_Kong.mp4", "resources/face_models/naomi_watts_2.png", "Naomi"],
    ["The Fall of Kong", "resources/video/The_Fall_of_Kong.mp4", "resources/face_models/naomi_watts_2.png", "Naomi"],
    ['The Fall of Kong (Short)', "resource/video/Kingkong_sort.mp4", "resources/face_models/naomi_watts_2.png", "Naomi"],
    ["Despacito", "resources/video/Despacito.mp4", "resources/face_models/Yankee.png", "Yankee"]
]

casc_path = "haarcascade_frontalface_default.xml"


titles = {
    'select_video':'Chọn video:',
    'search_face':'Tìm kiếm mặt',
    'search_match_face':'Nhận diện',
    'select_face_detection':'Chọn mặt nhận diện:',
    'message_select_video':'Vui lòng chọn video',
    'message_select_model':'Lựu chọn mặt nhận diện'
}

output = {
    'output': 'output',
    'face_all': 'all',
    'face_found': 'found'
}

folder = {
    'output': os.path.abspath('{}'.format(output['output'])),
    'face_matching_folder': os.path.abspath('output/{}'.format(output['face_found'])),
    'face_output_folder': os.path.abspath('output/{}'.format(output['face_all']))
}
